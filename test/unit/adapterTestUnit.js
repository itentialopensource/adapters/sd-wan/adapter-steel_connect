/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-steel_connect',
      type: 'SteelConnect',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const SteelConnect = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Steel_connect Adapter Test', () => {
  describe('SteelConnect Class Tests', () => {
    const a = new SteelConnect(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('steel_connect'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('steel_connect'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('SteelConnect', pronghornDotJson.export);
          assert.equal('Steel_connect', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-steel_connect', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('steel_connect'));
          assert.equal('SteelConnect', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-steel_connect', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-steel_connect', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getStatus - errors', () => {
      it('should have a getStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgs - errors', () => {
      it('should have a getOrgs function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgs - errors', () => {
      it('should have a postOrgs function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.postOrgs(null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgid - errors', () => {
      it('should have a getOrgOrgid function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgid(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putOrgOrgid - errors', () => {
      it('should have a putOrgOrgid function', (done) => {
        try {
          assert.equal(true, typeof a.putOrgOrgid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.putOrgOrgid(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putOrgOrgid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.putOrgOrgid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putOrgOrgid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrgOrgid - errors', () => {
      it('should have a deleteOrgOrgid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOrgOrgid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.deleteOrgOrgid(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteOrgOrgid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBgpneighs - errors', () => {
      it('should have a getBgpneighs function', (done) => {
        try {
          assert.equal(true, typeof a.getBgpneighs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBgpneighsBgpneighid - errors', () => {
      it('should have a getBgpneighsBgpneighid function', (done) => {
        try {
          assert.equal(true, typeof a.getBgpneighsBgpneighid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpneighid', (done) => {
        try {
          a.getBgpneighsBgpneighid(null, (data, error) => {
            try {
              const displayE = 'bgpneighid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getBgpneighsBgpneighid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBgpneighsBgpneighid - errors', () => {
      it('should have a deleteBgpneighsBgpneighid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBgpneighsBgpneighid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpneighid', (done) => {
        try {
          a.deleteBgpneighsBgpneighid(null, (data, error) => {
            try {
              const displayE = 'bgpneighid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteBgpneighsBgpneighid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putBgpneighsBgpneighid - errors', () => {
      it('should have a putBgpneighsBgpneighid function', (done) => {
        try {
          assert.equal(true, typeof a.putBgpneighsBgpneighid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpneighid', (done) => {
        try {
          a.putBgpneighsBgpneighid(null, (data, error) => {
            try {
              const displayE = 'bgpneighid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putBgpneighsBgpneighid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidBgpneighs - errors', () => {
      it('should have a getOrgOrgidBgpneighs function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidBgpneighs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidBgpneighs(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidBgpneighs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidBgpneighs - errors', () => {
      it('should have a postOrgOrgidBgpneighs function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidBgpneighs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidBgpneighs(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidBgpneighs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcinterfaces - errors', () => {
      it('should have a getDcinterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.getDcinterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcinterfacesDcinterfaceid - errors', () => {
      it('should have a getDcinterfacesDcinterfaceid function', (done) => {
        try {
          assert.equal(true, typeof a.getDcinterfacesDcinterfaceid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dcinterfaceid', (done) => {
        try {
          a.getDcinterfacesDcinterfaceid(null, (data, error) => {
            try {
              const displayE = 'dcinterfaceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getDcinterfacesDcinterfaceid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcinterfacesDcinterfaceid - errors', () => {
      it('should have a deleteDcinterfacesDcinterfaceid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcinterfacesDcinterfaceid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dcinterfaceid', (done) => {
        try {
          a.deleteDcinterfacesDcinterfaceid(null, (data, error) => {
            try {
              const displayE = 'dcinterfaceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteDcinterfacesDcinterfaceid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcinterfacesDcinterfaceid - errors', () => {
      it('should have a putDcinterfacesDcinterfaceid function', (done) => {
        try {
          assert.equal(true, typeof a.putDcinterfacesDcinterfaceid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dcinterfaceid', (done) => {
        try {
          a.putDcinterfacesDcinterfaceid(null, (data, error) => {
            try {
              const displayE = 'dcinterfaceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putDcinterfacesDcinterfaceid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidDcinterfaces - errors', () => {
      it('should have a getOrgOrgidDcinterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidDcinterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidDcinterfaces(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidDcinterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidDcinterfaces - errors', () => {
      it('should have a postOrgOrgidDcinterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidDcinterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidDcinterfaces(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidDcinterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUplinks - errors', () => {
      it('should have a getUplinks function', (done) => {
        try {
          assert.equal(true, typeof a.getUplinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUplinkUplinkid - errors', () => {
      it('should have a getUplinkUplinkid function', (done) => {
        try {
          assert.equal(true, typeof a.getUplinkUplinkid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uplinkid', (done) => {
        try {
          a.getUplinkUplinkid(null, (data, error) => {
            try {
              const displayE = 'uplinkid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getUplinkUplinkid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUplinkUplinkid - errors', () => {
      it('should have a putUplinkUplinkid function', (done) => {
        try {
          assert.equal(true, typeof a.putUplinkUplinkid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uplinkid', (done) => {
        try {
          a.putUplinkUplinkid(null, (data, error) => {
            try {
              const displayE = 'uplinkid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putUplinkUplinkid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUplinkUplinkid - errors', () => {
      it('should have a deleteUplinkUplinkid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUplinkUplinkid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uplinkid', (done) => {
        try {
          a.deleteUplinkUplinkid(null, (data, error) => {
            try {
              const displayE = 'uplinkid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteUplinkUplinkid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidUplinks - errors', () => {
      it('should have a getOrgOrgidUplinks function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidUplinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidUplinks(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidUplinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidUplinks - errors', () => {
      it('should have a postOrgOrgidUplinks function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidUplinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidUplinks(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidUplinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uplink', (done) => {
        try {
          a.postOrgOrgidUplinks('fakeparam', null, (data, error) => {
            try {
              const displayE = 'uplink is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidUplinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteSiteidUplinks - errors', () => {
      it('should have a getSiteSiteidUplinks function', (done) => {
        try {
          assert.equal(true, typeof a.getSiteSiteidUplinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteid', (done) => {
        try {
          a.getSiteSiteidUplinks(null, (data, error) => {
            try {
              const displayE = 'siteid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getSiteSiteidUplinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUplinkUplinkidState - errors', () => {
      it('should have a putUplinkUplinkidState function', (done) => {
        try {
          assert.equal(true, typeof a.putUplinkUplinkidState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uplinkid', (done) => {
        try {
          a.putUplinkUplinkidState(null, null, (data, error) => {
            try {
              const displayE = 'uplinkid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putUplinkUplinkidState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putUplinkUplinkidState('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putUplinkUplinkidState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSites - errors', () => {
      it('should have a getSites function', (done) => {
        try {
          assert.equal(true, typeof a.getSites === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidSites - errors', () => {
      it('should have a getOrgOrgidSites function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidSites === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidSites(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidSites', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidSites - errors', () => {
      it('should have a postOrgOrgidSites function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidSites === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidSites(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidSites', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing site', (done) => {
        try {
          a.postOrgOrgidSites('fakeparam', null, (data, error) => {
            try {
              const displayE = 'site is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidSites', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteSiteid - errors', () => {
      it('should have a getSiteSiteid function', (done) => {
        try {
          assert.equal(true, typeof a.getSiteSiteid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteid', (done) => {
        try {
          a.getSiteSiteid(null, (data, error) => {
            try {
              const displayE = 'siteid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getSiteSiteid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putSiteSiteid - errors', () => {
      it('should have a putSiteSiteid function', (done) => {
        try {
          assert.equal(true, typeof a.putSiteSiteid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteid', (done) => {
        try {
          a.putSiteSiteid(null, (data, error) => {
            try {
              const displayE = 'siteid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putSiteSiteid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSiteSiteid - errors', () => {
      it('should have a deleteSiteSiteid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSiteSiteid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteid', (done) => {
        try {
          a.deleteSiteSiteid(null, (data, error) => {
            try {
              const displayE = 'siteid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteSiteSiteid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSitegroups - errors', () => {
      it('should have a getSitegroups function', (done) => {
        try {
          assert.equal(true, typeof a.getSitegroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidSitegroups - errors', () => {
      it('should have a getOrgOrgidSitegroups function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidSitegroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidSitegroups(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidSitegroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidSitegroups - errors', () => {
      it('should have a postOrgOrgidSitegroups function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidSitegroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidSitegroups(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidSitegroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sitegroup', (done) => {
        try {
          a.postOrgOrgidSitegroups('fakeparam', null, (data, error) => {
            try {
              const displayE = 'sitegroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidSitegroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSitegroupSitegroupid - errors', () => {
      it('should have a getSitegroupSitegroupid function', (done) => {
        try {
          assert.equal(true, typeof a.getSitegroupSitegroupid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sitegroupid', (done) => {
        try {
          a.getSitegroupSitegroupid(null, (data, error) => {
            try {
              const displayE = 'sitegroupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getSitegroupSitegroupid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putSitegroupSitegroupid - errors', () => {
      it('should have a putSitegroupSitegroupid function', (done) => {
        try {
          assert.equal(true, typeof a.putSitegroupSitegroupid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sitegroupid', (done) => {
        try {
          a.putSitegroupSitegroupid(null, (data, error) => {
            try {
              const displayE = 'sitegroupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putSitegroupSitegroupid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSitegroupSitegroupid - errors', () => {
      it('should have a deleteSitegroupSitegroupid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSitegroupSitegroupid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sitegroupid', (done) => {
        try {
          a.deleteSitegroupSitegroupid(null, (data, error) => {
            try {
              const displayE = 'sitegroupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteSitegroupSitegroupid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZones - errors', () => {
      it('should have a getZones function', (done) => {
        try {
          assert.equal(true, typeof a.getZones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZoneZoneid - errors', () => {
      it('should have a getZoneZoneid function', (done) => {
        try {
          assert.equal(true, typeof a.getZoneZoneid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneid', (done) => {
        try {
          a.getZoneZoneid(null, (data, error) => {
            try {
              const displayE = 'zoneid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getZoneZoneid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putZoneZoneid - errors', () => {
      it('should have a putZoneZoneid function', (done) => {
        try {
          assert.equal(true, typeof a.putZoneZoneid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneid', (done) => {
        try {
          a.putZoneZoneid(null, (data, error) => {
            try {
              const displayE = 'zoneid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putZoneZoneid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteZoneZoneid - errors', () => {
      it('should have a deleteZoneZoneid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteZoneZoneid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneid', (done) => {
        try {
          a.deleteZoneZoneid(null, (data, error) => {
            try {
              const displayE = 'zoneid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteZoneZoneid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidZones - errors', () => {
      it('should have a getOrgOrgidZones function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidZones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidZones(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidZones', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidZones - errors', () => {
      it('should have a postOrgOrgidZones function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidZones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidZones(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidZones', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zone', (done) => {
        try {
          a.postOrgOrgidZones('fakeparam', null, (data, error) => {
            try {
              const displayE = 'zone is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidZones', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteSiteidZones - errors', () => {
      it('should have a getSiteSiteidZones function', (done) => {
        try {
          assert.equal(true, typeof a.getSiteSiteidZones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteid', (done) => {
        try {
          a.getSiteSiteidZones(null, (data, error) => {
            try {
              const displayE = 'siteid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getSiteSiteidZones', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwitches - errors', () => {
      it('should have a getSwitches function', (done) => {
        try {
          assert.equal(true, typeof a.getSwitches === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidSwitches - errors', () => {
      it('should have a getOrgOrgidSwitches function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidSwitches === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidSwitches(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidSwitches', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidSwitches - errors', () => {
      it('should have a postOrgOrgidSwitches function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidSwitches === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidSwitches(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidSwitches', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchParam', (done) => {
        try {
          a.postOrgOrgidSwitches('fakeparam', null, (data, error) => {
            try {
              const displayE = 'switchParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidSwitches', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwitchSwitchid - errors', () => {
      it('should have a getSwitchSwitchid function', (done) => {
        try {
          assert.equal(true, typeof a.getSwitchSwitchid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchid', (done) => {
        try {
          a.getSwitchSwitchid(null, (data, error) => {
            try {
              const displayE = 'switchid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getSwitchSwitchid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putSwitchSwitchid - errors', () => {
      it('should have a putSwitchSwitchid function', (done) => {
        try {
          assert.equal(true, typeof a.putSwitchSwitchid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchid', (done) => {
        try {
          a.putSwitchSwitchid(null, (data, error) => {
            try {
              const displayE = 'switchid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putSwitchSwitchid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSwitchSwitchid - errors', () => {
      it('should have a deleteSwitchSwitchid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSwitchSwitchid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchid', (done) => {
        try {
          a.deleteSwitchSwitchid(null, (data, error) => {
            try {
              const displayE = 'switchid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteSwitchSwitchid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAp - errors', () => {
      it('should have a getAp function', (done) => {
        try {
          assert.equal(true, typeof a.getAp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidAp - errors', () => {
      it('should have a getOrgOrgidAp function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidAp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidAp(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidAp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidAp - errors', () => {
      it('should have a postOrgOrgidAp function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidAp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidAp(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidAp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ap', (done) => {
        try {
          a.postOrgOrgidAp('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ap is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidAp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApApid - errors', () => {
      it('should have a getApApid function', (done) => {
        try {
          assert.equal(true, typeof a.getApApid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apid', (done) => {
        try {
          a.getApApid(null, (data, error) => {
            try {
              const displayE = 'apid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getApApid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putApApid - errors', () => {
      it('should have a putApApid function', (done) => {
        try {
          assert.equal(true, typeof a.putApApid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apid', (done) => {
        try {
          a.putApApid(null, (data, error) => {
            try {
              const displayE = 'apid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putApApid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApApid - errors', () => {
      it('should have a deleteApApid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApApid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apid', (done) => {
        try {
          a.deleteApApid(null, (data, error) => {
            try {
              const displayE = 'apid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteApApid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodes - errors', () => {
      it('should have a getNodes function', (done) => {
        try {
          assert.equal(true, typeof a.getNodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeNodeidImageStatus - errors', () => {
      it('should have a getNodeNodeidImageStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeNodeidImageStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getNodeNodeidImageStatus(null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getNodeNodeidImageStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeNodeidGetImage - errors', () => {
      it('should have a getNodeNodeidGetImage function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeNodeidGetImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing file', (done) => {
        try {
          a.getNodeNodeidGetImage(null, null, (data, error) => {
            try {
              const displayE = 'file is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getNodeNodeidGetImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getNodeNodeidGetImage('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getNodeNodeidGetImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeNodeid - errors', () => {
      it('should have a getNodeNodeid function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeNodeid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getNodeNodeid(null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getNodeNodeid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNodeNodeid - errors', () => {
      it('should have a putNodeNodeid function', (done) => {
        try {
          assert.equal(true, typeof a.putNodeNodeid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.putNodeNodeid(null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putNodeNodeid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNodeNodeid - errors', () => {
      it('should have a deleteNodeNodeid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNodeNodeid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.deleteNodeNodeid(null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteNodeNodeid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeNodeidRemove - errors', () => {
      it('should have a postNodeNodeidRemove function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeNodeidRemove === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.postNodeNodeidRemove(null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postNodeNodeidRemove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeNodeidFactoryReset - errors', () => {
      it('should have a postNodeNodeidFactoryReset function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeNodeidFactoryReset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.postNodeNodeidFactoryReset(null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postNodeNodeidFactoryReset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeNodeidReboot - errors', () => {
      it('should have a postNodeNodeidReboot function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeNodeidReboot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.postNodeNodeidReboot(null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postNodeNodeidReboot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidNodeRegister - errors', () => {
      it('should have a postOrgOrgidNodeRegister function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidNodeRegister === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidNodeRegister(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidNodeRegister', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing node', (done) => {
        try {
          a.postOrgOrgidNodeRegister('fakeparam', null, (data, error) => {
            try {
              const displayE = 'node is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidNodeRegister', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidNodeVirtualRegister - errors', () => {
      it('should have a postOrgOrgidNodeVirtualRegister function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidNodeVirtualRegister === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidNodeVirtualRegister(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidNodeVirtualRegister', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing node', (done) => {
        try {
          a.postOrgOrgidNodeVirtualRegister('fakeparam', null, (data, error) => {
            try {
              const displayE = 'node is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidNodeVirtualRegister', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeNodeidPrepareImage - errors', () => {
      it('should have a postNodeNodeidPrepareImage function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeNodeidPrepareImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.postNodeNodeidPrepareImage(null, null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postNodeNodeidPrepareImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.postNodeNodeidPrepareImage('fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postNodeNodeidPrepareImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeNodeidGenerateSupportPackage - errors', () => {
      it('should have a postNodeNodeidGenerateSupportPackage function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeNodeidGenerateSupportPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.postNodeNodeidGenerateSupportPackage(null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postNodeNodeidGenerateSupportPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeNodeidDownloadSupportPackage - errors', () => {
      it('should have a getNodeNodeidDownloadSupportPackage function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeNodeidDownloadSupportPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getNodeNodeidDownloadSupportPackage(null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getNodeNodeidDownloadSupportPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeNodeidInventoryUpdate - errors', () => {
      it('should have a postNodeNodeidInventoryUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeNodeidInventoryUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.postNodeNodeidInventoryUpdate(null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postNodeNodeidInventoryUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteSiteidNodes - errors', () => {
      it('should have a getSiteSiteidNodes function', (done) => {
        try {
          assert.equal(true, typeof a.getSiteSiteidNodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteid', (done) => {
        try {
          a.getSiteSiteidNodes(null, (data, error) => {
            try {
              const displayE = 'siteid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getSiteSiteidNodes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidNodes - errors', () => {
      it('should have a getOrgOrgidNodes function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidNodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidNodes(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidNodes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeNodeidRetryUpgrade - errors', () => {
      it('should have a postNodeNodeidRetryUpgrade function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeNodeidRetryUpgrade === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.postNodeNodeidRetryUpgrade(null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postNodeNodeidRetryUpgrade', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusters - errors', () => {
      it('should have a getClusters function', (done) => {
        try {
          assert.equal(true, typeof a.getClusters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusterClusterid - errors', () => {
      it('should have a getClusterClusterid function', (done) => {
        try {
          assert.equal(true, typeof a.getClusterClusterid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clusterid', (done) => {
        try {
          a.getClusterClusterid(null, (data, error) => {
            try {
              const displayE = 'clusterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getClusterClusterid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putClusterClusterid - errors', () => {
      it('should have a putClusterClusterid function', (done) => {
        try {
          assert.equal(true, typeof a.putClusterClusterid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clusterid', (done) => {
        try {
          a.putClusterClusterid(null, null, (data, error) => {
            try {
              const displayE = 'clusterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putClusterClusterid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cluster', (done) => {
        try {
          a.putClusterClusterid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cluster is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putClusterClusterid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteClusterClusterid - errors', () => {
      it('should have a deleteClusterClusterid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteClusterClusterid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clusterid', (done) => {
        try {
          a.deleteClusterClusterid(null, (data, error) => {
            try {
              const displayE = 'clusterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteClusterClusterid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteSiteidClusters - errors', () => {
      it('should have a getSiteSiteidClusters function', (done) => {
        try {
          assert.equal(true, typeof a.getSiteSiteidClusters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteid', (done) => {
        try {
          a.getSiteSiteidClusters(null, (data, error) => {
            try {
              const displayE = 'siteid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getSiteSiteidClusters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidClusters - errors', () => {
      it('should have a getOrgOrgidClusters function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidClusters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidClusters(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidClusters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidClusters - errors', () => {
      it('should have a postOrgOrgidClusters function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidClusters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidClusters(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidClusters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cluster', (done) => {
        try {
          a.postOrgOrgidClusters('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cluster is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidClusters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcuplinks - errors', () => {
      it('should have a getDcuplinks function', (done) => {
        try {
          assert.equal(true, typeof a.getDcuplinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcuplinkDcuplinkid - errors', () => {
      it('should have a getDcuplinkDcuplinkid function', (done) => {
        try {
          assert.equal(true, typeof a.getDcuplinkDcuplinkid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dcuplinkid', (done) => {
        try {
          a.getDcuplinkDcuplinkid(null, (data, error) => {
            try {
              const displayE = 'dcuplinkid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getDcuplinkDcuplinkid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcuplinkDcuplinkid - errors', () => {
      it('should have a putDcuplinkDcuplinkid function', (done) => {
        try {
          assert.equal(true, typeof a.putDcuplinkDcuplinkid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dcuplinkid', (done) => {
        try {
          a.putDcuplinkDcuplinkid(null, null, (data, error) => {
            try {
              const displayE = 'dcuplinkid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putDcuplinkDcuplinkid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dcuplink', (done) => {
        try {
          a.putDcuplinkDcuplinkid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'dcuplink is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putDcuplinkDcuplinkid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcuplinkDcuplinkid - errors', () => {
      it('should have a deleteDcuplinkDcuplinkid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcuplinkDcuplinkid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dcuplinkid', (done) => {
        try {
          a.deleteDcuplinkDcuplinkid(null, (data, error) => {
            try {
              const displayE = 'dcuplinkid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteDcuplinkDcuplinkid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidDcuplinks - errors', () => {
      it('should have a getOrgOrgidDcuplinks function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidDcuplinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidDcuplinks(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidDcuplinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidDcuplinks - errors', () => {
      it('should have a postOrgOrgidDcuplinks function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidDcuplinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidDcuplinks(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidDcuplinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dcuplink', (done) => {
        try {
          a.postOrgOrgidDcuplinks('fakeparam', null, (data, error) => {
            try {
              const displayE = 'dcuplink is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidDcuplinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSsids - errors', () => {
      it('should have a getSsids function', (done) => {
        try {
          assert.equal(true, typeof a.getSsids === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidSsids - errors', () => {
      it('should have a getOrgOrgidSsids function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidSsids === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidSsids(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidSsids', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidSsids - errors', () => {
      it('should have a postOrgOrgidSsids function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidSsids === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidSsids(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidSsids', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ssid', (done) => {
        try {
          a.postOrgOrgidSsids('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ssid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidSsids', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSsidSsidid - errors', () => {
      it('should have a getSsidSsidid function', (done) => {
        try {
          assert.equal(true, typeof a.getSsidSsidid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ssidid', (done) => {
        try {
          a.getSsidSsidid(null, (data, error) => {
            try {
              const displayE = 'ssidid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getSsidSsidid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putSsidSsidid - errors', () => {
      it('should have a putSsidSsidid function', (done) => {
        try {
          assert.equal(true, typeof a.putSsidSsidid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ssidid', (done) => {
        try {
          a.putSsidSsidid(null, (data, error) => {
            try {
              const displayE = 'ssidid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putSsidSsidid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSsidSsidid - errors', () => {
      it('should have a deleteSsidSsidid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSsidSsidid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ssidid', (done) => {
        try {
          a.deleteSsidSsidid(null, (data, error) => {
            try {
              const displayE = 'ssidid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteSsidSsidid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBroadcasts - errors', () => {
      it('should have a getBroadcasts function', (done) => {
        try {
          assert.equal(true, typeof a.getBroadcasts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidBroadcasts - errors', () => {
      it('should have a getOrgOrgidBroadcasts function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidBroadcasts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidBroadcasts(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidBroadcasts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidBroadcasts - errors', () => {
      it('should have a postOrgOrgidBroadcasts function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidBroadcasts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidBroadcasts(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidBroadcasts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing broadcast', (done) => {
        try {
          a.postOrgOrgidBroadcasts('fakeparam', null, (data, error) => {
            try {
              const displayE = 'broadcast is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidBroadcasts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteSiteidBroadcasts - errors', () => {
      it('should have a getSiteSiteidBroadcasts function', (done) => {
        try {
          assert.equal(true, typeof a.getSiteSiteidBroadcasts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteid', (done) => {
        try {
          a.getSiteSiteidBroadcasts(null, (data, error) => {
            try {
              const displayE = 'siteid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getSiteSiteidBroadcasts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBroadcastBcastid - errors', () => {
      it('should have a getBroadcastBcastid function', (done) => {
        try {
          assert.equal(true, typeof a.getBroadcastBcastid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bcastid', (done) => {
        try {
          a.getBroadcastBcastid(null, (data, error) => {
            try {
              const displayE = 'bcastid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getBroadcastBcastid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putBroadcastBcastid - errors', () => {
      it('should have a putBroadcastBcastid function', (done) => {
        try {
          assert.equal(true, typeof a.putBroadcastBcastid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bcastid', (done) => {
        try {
          a.putBroadcastBcastid(null, null, (data, error) => {
            try {
              const displayE = 'bcastid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putBroadcastBcastid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing broadcast', (done) => {
        try {
          a.putBroadcastBcastid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'broadcast is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putBroadcastBcastid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBroadcastBcastid - errors', () => {
      it('should have a deleteBroadcastBcastid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBroadcastBcastid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bcastid', (done) => {
        try {
          a.deleteBroadcastBcastid(null, (data, error) => {
            try {
              const displayE = 'bcastid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteBroadcastBcastid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApps - errors', () => {
      it('should have a getApps function', (done) => {
        try {
          assert.equal(true, typeof a.getApps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppAppid - errors', () => {
      it('should have a getAppAppid function', (done) => {
        try {
          assert.equal(true, typeof a.getAppAppid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appid', (done) => {
        try {
          a.getAppAppid(null, (data, error) => {
            try {
              const displayE = 'appid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getAppAppid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomApps - errors', () => {
      it('should have a getCustomApps function', (done) => {
        try {
          assert.equal(true, typeof a.getCustomApps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomAppAppid - errors', () => {
      it('should have a getCustomAppAppid function', (done) => {
        try {
          assert.equal(true, typeof a.getCustomAppAppid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appid', (done) => {
        try {
          a.getCustomAppAppid(null, (data, error) => {
            try {
              const displayE = 'appid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getCustomAppAppid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCustomAppAppid - errors', () => {
      it('should have a putCustomAppAppid function', (done) => {
        try {
          assert.equal(true, typeof a.putCustomAppAppid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appid', (done) => {
        try {
          a.putCustomAppAppid(null, (data, error) => {
            try {
              const displayE = 'appid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putCustomAppAppid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomAppAppid - errors', () => {
      it('should have a deleteCustomAppAppid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCustomAppAppid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appid', (done) => {
        try {
          a.deleteCustomAppAppid(null, (data, error) => {
            try {
              const displayE = 'appid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteCustomAppAppid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidCustomApps - errors', () => {
      it('should have a getOrgOrgidCustomApps function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidCustomApps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidCustomApps(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidCustomApps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidCustomApps - errors', () => {
      it('should have a postOrgOrgidCustomApps function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidCustomApps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidCustomApps(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidCustomApps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customApp', (done) => {
        try {
          a.postOrgOrgidCustomApps('fakeparam', null, (data, error) => {
            try {
              const displayE = 'customApp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidCustomApps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppGroups - errors', () => {
      it('should have a getAppGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getAppGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppGroupAppgrpid - errors', () => {
      it('should have a getAppGroupAppgrpid function', (done) => {
        try {
          assert.equal(true, typeof a.getAppGroupAppgrpid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appgrpid', (done) => {
        try {
          a.getAppGroupAppgrpid(null, (data, error) => {
            try {
              const displayE = 'appgrpid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getAppGroupAppgrpid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putAppGroupAppgrpid - errors', () => {
      it('should have a putAppGroupAppgrpid function', (done) => {
        try {
          assert.equal(true, typeof a.putAppGroupAppgrpid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appgrpid', (done) => {
        try {
          a.putAppGroupAppgrpid(null, (data, error) => {
            try {
              const displayE = 'appgrpid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putAppGroupAppgrpid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppGroupAppgrpid - errors', () => {
      it('should have a deleteAppGroupAppgrpid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppGroupAppgrpid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appgrpid', (done) => {
        try {
          a.deleteAppGroupAppgrpid(null, (data, error) => {
            try {
              const displayE = 'appgrpid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteAppGroupAppgrpid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidAppGroups - errors', () => {
      it('should have a getOrgOrgidAppGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidAppGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidAppGroups(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidAppGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidAppGroups - errors', () => {
      it('should have a postOrgOrgidAppGroups function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidAppGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidAppGroups(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidAppGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appgrp', (done) => {
        try {
          a.postOrgOrgidAppGroups('fakeparam', null, (data, error) => {
            try {
              const displayE = 'appgrp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidAppGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsers - errors', () => {
      it('should have a getUsers function', (done) => {
        try {
          assert.equal(true, typeof a.getUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidUsers - errors', () => {
      it('should have a getOrgOrgidUsers function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidUsers(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidUsers - errors', () => {
      it('should have a postOrgOrgidUsers function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidUsers(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing user', (done) => {
        try {
          a.postOrgOrgidUsers('fakeparam', null, (data, error) => {
            try {
              const displayE = 'user is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserUserid - errors', () => {
      it('should have a getUserUserid function', (done) => {
        try {
          assert.equal(true, typeof a.getUserUserid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userid', (done) => {
        try {
          a.getUserUserid(null, (data, error) => {
            try {
              const displayE = 'userid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getUserUserid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUserUserid - errors', () => {
      it('should have a putUserUserid function', (done) => {
        try {
          assert.equal(true, typeof a.putUserUserid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userid', (done) => {
        try {
          a.putUserUserid(null, (data, error) => {
            try {
              const displayE = 'userid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putUserUserid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUserUserid - errors', () => {
      it('should have a deleteUserUserid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUserUserid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userid', (done) => {
        try {
          a.deleteUserUserid(null, (data, error) => {
            try {
              const displayE = 'userid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteUserUserid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevices - errors', () => {
      it('should have a getDevices function', (done) => {
        try {
          assert.equal(true, typeof a.getDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceDevid - errors', () => {
      it('should have a getDeviceDevid function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceDevid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing devid', (done) => {
        try {
          a.getDeviceDevid(null, (data, error) => {
            try {
              const displayE = 'devid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getDeviceDevid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDeviceDevid - errors', () => {
      it('should have a putDeviceDevid function', (done) => {
        try {
          assert.equal(true, typeof a.putDeviceDevid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing devid', (done) => {
        try {
          a.putDeviceDevid(null, (data, error) => {
            try {
              const displayE = 'devid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putDeviceDevid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceDevid - errors', () => {
      it('should have a deleteDeviceDevid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDeviceDevid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing devid', (done) => {
        try {
          a.deleteDeviceDevid(null, (data, error) => {
            try {
              const displayE = 'devid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteDeviceDevid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidDevices - errors', () => {
      it('should have a getOrgOrgidDevices function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidDevices(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidDevices - errors', () => {
      it('should have a postOrgOrgidDevices function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidDevices(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.postOrgOrgidDevices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPathRules - errors', () => {
      it('should have a getPathRules function', (done) => {
        try {
          assert.equal(true, typeof a.getPathRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPathRulePruleid - errors', () => {
      it('should have a getPathRulePruleid function', (done) => {
        try {
          assert.equal(true, typeof a.getPathRulePruleid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pruleid', (done) => {
        try {
          a.getPathRulePruleid(null, (data, error) => {
            try {
              const displayE = 'pruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getPathRulePruleid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPathRulePruleid - errors', () => {
      it('should have a putPathRulePruleid function', (done) => {
        try {
          assert.equal(true, typeof a.putPathRulePruleid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pruleid', (done) => {
        try {
          a.putPathRulePruleid(null, (data, error) => {
            try {
              const displayE = 'pruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putPathRulePruleid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePathRulePruleid - errors', () => {
      it('should have a deletePathRulePruleid function', (done) => {
        try {
          assert.equal(true, typeof a.deletePathRulePruleid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pruleid', (done) => {
        try {
          a.deletePathRulePruleid(null, (data, error) => {
            try {
              const displayE = 'pruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deletePathRulePruleid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidPathRules - errors', () => {
      it('should have a getOrgOrgidPathRules function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidPathRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidPathRules(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidPathRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidPathRules - errors', () => {
      it('should have a postOrgOrgidPathRules function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidPathRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidPathRules(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidPathRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathrule', (done) => {
        try {
          a.postOrgOrgidPathRules('fakeparam', null, (data, error) => {
            try {
              const displayE = 'pathrule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidPathRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOutboundRules - errors', () => {
      it('should have a getOutboundRules function', (done) => {
        try {
          assert.equal(true, typeof a.getOutboundRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOutboundRuleRuleid - errors', () => {
      it('should have a getOutboundRuleRuleid function', (done) => {
        try {
          assert.equal(true, typeof a.getOutboundRuleRuleid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleid', (done) => {
        try {
          a.getOutboundRuleRuleid(null, (data, error) => {
            try {
              const displayE = 'ruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOutboundRuleRuleid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putOutboundRuleRuleid - errors', () => {
      it('should have a putOutboundRuleRuleid function', (done) => {
        try {
          assert.equal(true, typeof a.putOutboundRuleRuleid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleid', (done) => {
        try {
          a.putOutboundRuleRuleid(null, (data, error) => {
            try {
              const displayE = 'ruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putOutboundRuleRuleid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOutboundRuleRuleid - errors', () => {
      it('should have a deleteOutboundRuleRuleid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOutboundRuleRuleid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleid', (done) => {
        try {
          a.deleteOutboundRuleRuleid(null, (data, error) => {
            try {
              const displayE = 'ruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteOutboundRuleRuleid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidOutboundRules - errors', () => {
      it('should have a getOrgOrgidOutboundRules function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidOutboundRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidOutboundRules(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidOutboundRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidOutboundRules - errors', () => {
      it('should have a postOrgOrgidOutboundRules function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidOutboundRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidOutboundRules(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidOutboundRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing outbound', (done) => {
        try {
          a.postOrgOrgidOutboundRules('fakeparam', null, (data, error) => {
            try {
              const displayE = 'outbound is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidOutboundRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInboundRules - errors', () => {
      it('should have a getInboundRules function', (done) => {
        try {
          assert.equal(true, typeof a.getInboundRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidInboundRules - errors', () => {
      it('should have a getOrgOrgidInboundRules function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidInboundRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidInboundRules(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidInboundRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidInboundRules - errors', () => {
      it('should have a postOrgOrgidInboundRules function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidInboundRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidInboundRules(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidInboundRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing inboundrule', (done) => {
        try {
          a.postOrgOrgidInboundRules('fakeparam', null, (data, error) => {
            try {
              const displayE = 'inboundrule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidInboundRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInboundRuleRuleid - errors', () => {
      it('should have a getInboundRuleRuleid function', (done) => {
        try {
          assert.equal(true, typeof a.getInboundRuleRuleid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleid', (done) => {
        try {
          a.getInboundRuleRuleid(null, (data, error) => {
            try {
              const displayE = 'ruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getInboundRuleRuleid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putInboundRuleRuleid - errors', () => {
      it('should have a putInboundRuleRuleid function', (done) => {
        try {
          assert.equal(true, typeof a.putInboundRuleRuleid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleid', (done) => {
        try {
          a.putInboundRuleRuleid(null, (data, error) => {
            try {
              const displayE = 'ruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putInboundRuleRuleid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInboundRuleRuleid - errors', () => {
      it('should have a deleteInboundRuleRuleid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInboundRuleRuleid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleid', (done) => {
        try {
          a.deleteInboundRuleRuleid(null, (data, error) => {
            try {
              const displayE = 'ruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteInboundRuleRuleid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEndpoints - errors', () => {
      it('should have a getEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.getEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidEndpoints - errors', () => {
      it('should have a getOrgOrgidEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidEndpoints(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidEndpoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidEndpoints - errors', () => {
      it('should have a postOrgOrgidEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidEndpoints(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidEndpoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpoint', (done) => {
        try {
          a.postOrgOrgidEndpoints('fakeparam', null, (data, error) => {
            try {
              const displayE = 'endpoint is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidEndpoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEndpointEpid - errors', () => {
      it('should have a getEndpointEpid function', (done) => {
        try {
          assert.equal(true, typeof a.getEndpointEpid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing epid', (done) => {
        try {
          a.getEndpointEpid(null, (data, error) => {
            try {
              const displayE = 'epid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getEndpointEpid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putEndpointEpid - errors', () => {
      it('should have a putEndpointEpid function', (done) => {
        try {
          assert.equal(true, typeof a.putEndpointEpid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing epid', (done) => {
        try {
          a.putEndpointEpid(null, (data, error) => {
            try {
              const displayE = 'epid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putEndpointEpid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEndpointEpid - errors', () => {
      it('should have a deleteEndpointEpid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteEndpointEpid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing epid', (done) => {
        try {
          a.deleteEndpointEpid(null, (data, error) => {
            try {
              const displayE = 'epid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteEndpointEpid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworks - errors', () => {
      it('should have a getNetworks function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidNetworks - errors', () => {
      it('should have a getOrgOrgidNetworks function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidNetworks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidNetworks(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidNetworks - errors', () => {
      it('should have a postOrgOrgidNetworks function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidNetworks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidNetworks(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing network', (done) => {
        try {
          a.postOrgOrgidNetworks('fakeparam', null, (data, error) => {
            try {
              const displayE = 'network is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkNetid - errors', () => {
      it('should have a getNetworkNetid function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkNetid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing netid', (done) => {
        try {
          a.getNetworkNetid(null, (data, error) => {
            try {
              const displayE = 'netid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getNetworkNetid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNetworkNetid - errors', () => {
      it('should have a putNetworkNetid function', (done) => {
        try {
          assert.equal(true, typeof a.putNetworkNetid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing netid', (done) => {
        try {
          a.putNetworkNetid(null, (data, error) => {
            try {
              const displayE = 'netid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putNetworkNetid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkNetid - errors', () => {
      it('should have a deleteNetworkNetid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkNetid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing netid', (done) => {
        try {
          a.deleteNetworkNetid(null, (data, error) => {
            try {
              const displayE = 'netid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteNetworkNetid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPorts - errors', () => {
      it('should have a getPorts function', (done) => {
        try {
          assert.equal(true, typeof a.getPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortPortid - errors', () => {
      it('should have a getPortPortid function', (done) => {
        try {
          assert.equal(true, typeof a.getPortPortid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portid', (done) => {
        try {
          a.getPortPortid(null, (data, error) => {
            try {
              const displayE = 'portid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getPortPortid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPortPortid - errors', () => {
      it('should have a putPortPortid function', (done) => {
        try {
          assert.equal(true, typeof a.putPortPortid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portid', (done) => {
        try {
          a.putPortPortid(null, (data, error) => {
            try {
              const displayE = 'portid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putPortPortid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeNodeidPorts - errors', () => {
      it('should have a getNodeNodeidPorts function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeNodeidPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getNodeNodeidPorts(null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getNodeNodeidPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWans - errors', () => {
      it('should have a getWans function', (done) => {
        try {
          assert.equal(true, typeof a.getWans === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidWans - errors', () => {
      it('should have a getOrgOrgidWans function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidWans === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidWans(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidWans', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidWans - errors', () => {
      it('should have a postOrgOrgidWans function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidWans === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidWans(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidWans', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wan', (done) => {
        try {
          a.postOrgOrgidWans('fakeparam', null, (data, error) => {
            try {
              const displayE = 'wan is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidWans', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWanWanid - errors', () => {
      it('should have a getWanWanid function', (done) => {
        try {
          assert.equal(true, typeof a.getWanWanid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wanid', (done) => {
        try {
          a.getWanWanid(null, (data, error) => {
            try {
              const displayE = 'wanid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getWanWanid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWanWanid - errors', () => {
      it('should have a putWanWanid function', (done) => {
        try {
          assert.equal(true, typeof a.putWanWanid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wanid', (done) => {
        try {
          a.putWanWanid(null, (data, error) => {
            try {
              const displayE = 'wanid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putWanWanid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWanWanid - errors', () => {
      it('should have a deleteWanWanid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWanWanid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wanid', (done) => {
        try {
          a.deleteWanWanid(null, (data, error) => {
            try {
              const displayE = 'wanid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteWanWanid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSshtunnelNodeid - errors', () => {
      it('should have a postSshtunnelNodeid function', (done) => {
        try {
          assert.equal(true, typeof a.postSshtunnelNodeid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.postSshtunnelNodeid(null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postSshtunnelNodeid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSshtunnelNodeid - errors', () => {
      it('should have a getSshtunnelNodeid function', (done) => {
        try {
          assert.equal(true, typeof a.getSshtunnelNodeid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getSshtunnelNodeid(null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getSshtunnelNodeid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSshtunnelNodeid - errors', () => {
      it('should have a deleteSshtunnelNodeid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSshtunnelNodeid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.deleteSshtunnelNodeid(null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteSshtunnelNodeid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSshtunnel - errors', () => {
      it('should have a getSshtunnel function', (done) => {
        try {
          assert.equal(true, typeof a.getSshtunnel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProxyservices - errors', () => {
      it('should have a getProxyservices function', (done) => {
        try {
          assert.equal(true, typeof a.getProxyservices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProxyserviceProxyserviceid - errors', () => {
      it('should have a getProxyserviceProxyserviceid function', (done) => {
        try {
          assert.equal(true, typeof a.getProxyserviceProxyserviceid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing proxyserviceid', (done) => {
        try {
          a.getProxyserviceProxyserviceid(null, (data, error) => {
            try {
              const displayE = 'proxyserviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getProxyserviceProxyserviceid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteProxyserviceProxyserviceid - errors', () => {
      it('should have a deleteProxyserviceProxyserviceid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteProxyserviceProxyserviceid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing proxyserviceid', (done) => {
        try {
          a.deleteProxyserviceProxyserviceid(null, (data, error) => {
            try {
              const displayE = 'proxyserviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteProxyserviceProxyserviceid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putProxyserviceProxyserviceid - errors', () => {
      it('should have a putProxyserviceProxyserviceid function', (done) => {
        try {
          assert.equal(true, typeof a.putProxyserviceProxyserviceid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing proxyserviceid', (done) => {
        try {
          a.putProxyserviceProxyserviceid(null, (data, error) => {
            try {
              const displayE = 'proxyserviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putProxyserviceProxyserviceid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidProxyservices - errors', () => {
      it('should have a getOrgOrgidProxyservices function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidProxyservices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidProxyservices(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidProxyservices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidProxyservices - errors', () => {
      it('should have a postOrgOrgidProxyservices function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidProxyservices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidProxyservices(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidProxyservices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidProxyserviceSettings - errors', () => {
      it('should have a getOrgOrgidProxyserviceSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidProxyserviceSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidProxyserviceSettings(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidProxyserviceSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidProxyserviceSettings - errors', () => {
      it('should have a postOrgOrgidProxyserviceSettings function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidProxyserviceSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidProxyserviceSettings(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidProxyserviceSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidProxyserviceConfig - errors', () => {
      it('should have a getOrgOrgidProxyserviceConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidProxyserviceConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidProxyserviceConfig(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidProxyserviceConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postProxyserviceCommand - errors', () => {
      it('should have a postProxyserviceCommand function', (done) => {
        try {
          assert.equal(true, typeof a.postProxyserviceCommand === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsergrps - errors', () => {
      it('should have a getUsergrps function', (done) => {
        try {
          assert.equal(true, typeof a.getUsergrps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsergrpUsergrpid - errors', () => {
      it('should have a getUsergrpUsergrpid function', (done) => {
        try {
          assert.equal(true, typeof a.getUsergrpUsergrpid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing usergrpid', (done) => {
        try {
          a.getUsergrpUsergrpid(null, (data, error) => {
            try {
              const displayE = 'usergrpid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getUsergrpUsergrpid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUsergrpUsergrpid - errors', () => {
      it('should have a putUsergrpUsergrpid function', (done) => {
        try {
          assert.equal(true, typeof a.putUsergrpUsergrpid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing usergrpid', (done) => {
        try {
          a.putUsergrpUsergrpid(null, (data, error) => {
            try {
              const displayE = 'usergrpid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putUsergrpUsergrpid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsergrpUsergrpid - errors', () => {
      it('should have a deleteUsergrpUsergrpid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUsergrpUsergrpid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing usergrpid', (done) => {
        try {
          a.deleteUsergrpUsergrpid(null, (data, error) => {
            try {
              const displayE = 'usergrpid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteUsergrpUsergrpid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidUsergrps - errors', () => {
      it('should have a getOrgOrgidUsergrps function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidUsergrps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidUsergrps(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidUsergrps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidUsergrps - errors', () => {
      it('should have a postOrgOrgidUsergrps function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidUsergrps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidUsergrps(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidUsergrps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing usergrp', (done) => {
        try {
          a.postOrgOrgidUsergrps('fakeparam', null, (data, error) => {
            try {
              const displayE = 'usergrp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidUsergrps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortal - errors', () => {
      it('should have a getPortal function', (done) => {
        try {
          assert.equal(true, typeof a.getPortal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortalPortalid - errors', () => {
      it('should have a getPortalPortalid function', (done) => {
        try {
          assert.equal(true, typeof a.getPortalPortalid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portalid', (done) => {
        try {
          a.getPortalPortalid(null, (data, error) => {
            try {
              const displayE = 'portalid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getPortalPortalid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPortalPortalid - errors', () => {
      it('should have a putPortalPortalid function', (done) => {
        try {
          assert.equal(true, typeof a.putPortalPortalid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portalid', (done) => {
        try {
          a.putPortalPortalid(null, (data, error) => {
            try {
              const displayE = 'portalid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putPortalPortalid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePortalPortalid - errors', () => {
      it('should have a deletePortalPortalid function', (done) => {
        try {
          assert.equal(true, typeof a.deletePortalPortalid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portalid', (done) => {
        try {
          a.deletePortalPortalid(null, (data, error) => {
            try {
              const displayE = 'portalid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deletePortalPortalid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidPortals - errors', () => {
      it('should have a getOrgOrgidPortals function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidPortals === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidPortals(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidPortals', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidPortals - errors', () => {
      it('should have a postOrgOrgidPortals function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidPortals === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidPortals(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidPortals', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portal', (done) => {
        try {
          a.postOrgOrgidPortals('fakeparam', null, (data, error) => {
            try {
              const displayE = 'portal is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidPortals', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPortalPortalidPortalLogo - errors', () => {
      it('should have a postPortalPortalidPortalLogo function', (done) => {
        try {
          assert.equal(true, typeof a.postPortalPortalidPortalLogo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portalid', (done) => {
        try {
          a.postPortalPortalidPortalLogo(null, (data, error) => {
            try {
              const displayE = 'portalid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postPortalPortalidPortalLogo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOspfnets - errors', () => {
      it('should have a getOspfnets function', (done) => {
        try {
          assert.equal(true, typeof a.getOspfnets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOspfnetOspfnetid - errors', () => {
      it('should have a getOspfnetOspfnetid function', (done) => {
        try {
          assert.equal(true, typeof a.getOspfnetOspfnetid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ospfnetid', (done) => {
        try {
          a.getOspfnetOspfnetid(null, (data, error) => {
            try {
              const displayE = 'ospfnetid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOspfnetOspfnetid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putOspfnetOspfnetid - errors', () => {
      it('should have a putOspfnetOspfnetid function', (done) => {
        try {
          assert.equal(true, typeof a.putOspfnetOspfnetid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ospfnetid', (done) => {
        try {
          a.putOspfnetOspfnetid(null, (data, error) => {
            try {
              const displayE = 'ospfnetid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putOspfnetOspfnetid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOspfnetOspfnetid - errors', () => {
      it('should have a deleteOspfnetOspfnetid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOspfnetOspfnetid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ospfnetid', (done) => {
        try {
          a.deleteOspfnetOspfnetid(null, (data, error) => {
            try {
              const displayE = 'ospfnetid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteOspfnetOspfnetid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidOspfnets - errors', () => {
      it('should have a getOrgOrgidOspfnets function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidOspfnets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidOspfnets(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidOspfnets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidOspfnets - errors', () => {
      it('should have a postOrgOrgidOspfnets function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidOspfnets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidOspfnets(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidOspfnets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ospfnet', (done) => {
        try {
          a.postOrgOrgidOspfnets('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ospfnet is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidOspfnets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRollingUpgrades - errors', () => {
      it('should have a getRollingUpgrades function', (done) => {
        try {
          assert.equal(true, typeof a.getRollingUpgrades === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRollingUpgradeScheduleid - errors', () => {
      it('should have a getRollingUpgradeScheduleid function', (done) => {
        try {
          assert.equal(true, typeof a.getRollingUpgradeScheduleid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scheduleid', (done) => {
        try {
          a.getRollingUpgradeScheduleid(null, (data, error) => {
            try {
              const displayE = 'scheduleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getRollingUpgradeScheduleid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRollingUpgradeScheduleid - errors', () => {
      it('should have a putRollingUpgradeScheduleid function', (done) => {
        try {
          assert.equal(true, typeof a.putRollingUpgradeScheduleid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scheduleid', (done) => {
        try {
          a.putRollingUpgradeScheduleid(null, (data, error) => {
            try {
              const displayE = 'scheduleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putRollingUpgradeScheduleid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRollingUpgradeScheduleid - errors', () => {
      it('should have a deleteRollingUpgradeScheduleid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRollingUpgradeScheduleid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scheduleid', (done) => {
        try {
          a.deleteRollingUpgradeScheduleid(null, (data, error) => {
            try {
              const displayE = 'scheduleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteRollingUpgradeScheduleid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidRollingUpgrades - errors', () => {
      it('should have a getOrgOrgidRollingUpgrades function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidRollingUpgrades === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidRollingUpgrades(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidRollingUpgrades', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidRollingUpgrades - errors', () => {
      it('should have a postOrgOrgidRollingUpgrades function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidRollingUpgrades === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidRollingUpgrades(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidRollingUpgrades', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rollingUpgrade', (done) => {
        try {
          a.postOrgOrgidRollingUpgrades('fakeparam', null, (data, error) => {
            try {
              const displayE = 'rollingUpgrade is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidRollingUpgrades', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWifiPlans - errors', () => {
      it('should have a getWifiPlans function', (done) => {
        try {
          assert.equal(true, typeof a.getWifiPlans === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWifiPlanWifiPlanid - errors', () => {
      it('should have a getWifiPlanWifiPlanid function', (done) => {
        try {
          assert.equal(true, typeof a.getWifiPlanWifiPlanid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wifiPlanid', (done) => {
        try {
          a.getWifiPlanWifiPlanid(null, (data, error) => {
            try {
              const displayE = 'wifiPlanid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getWifiPlanWifiPlanid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWifiPlanWifiPlanid - errors', () => {
      it('should have a putWifiPlanWifiPlanid function', (done) => {
        try {
          assert.equal(true, typeof a.putWifiPlanWifiPlanid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wifiPlanid', (done) => {
        try {
          a.putWifiPlanWifiPlanid(null, (data, error) => {
            try {
              const displayE = 'wifiPlanid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putWifiPlanWifiPlanid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWifiPlanWifiPlanid - errors', () => {
      it('should have a deleteWifiPlanWifiPlanid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWifiPlanWifiPlanid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wifiPlanid', (done) => {
        try {
          a.deleteWifiPlanWifiPlanid(null, (data, error) => {
            try {
              const displayE = 'wifiPlanid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteWifiPlanWifiPlanid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidWifiPlans - errors', () => {
      it('should have a getOrgOrgidWifiPlans function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidWifiPlans === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidWifiPlans(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidWifiPlans', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidWifiPlans - errors', () => {
      it('should have a postOrgOrgidWifiPlans function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidWifiPlans === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidWifiPlans(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidWifiPlans', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wifiPlan', (done) => {
        try {
          a.postOrgOrgidWifiPlans('fakeparam', null, (data, error) => {
            try {
              const displayE = 'wifiPlan is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidWifiPlans', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCvpns - errors', () => {
      it('should have a getCvpns function', (done) => {
        try {
          assert.equal(true, typeof a.getCvpns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCvpnCvpnid - errors', () => {
      it('should have a getCvpnCvpnid function', (done) => {
        try {
          assert.equal(true, typeof a.getCvpnCvpnid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cvpnid', (done) => {
        try {
          a.getCvpnCvpnid(null, (data, error) => {
            try {
              const displayE = 'cvpnid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getCvpnCvpnid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCvpnCvpnid - errors', () => {
      it('should have a putCvpnCvpnid function', (done) => {
        try {
          assert.equal(true, typeof a.putCvpnCvpnid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cvpnid', (done) => {
        try {
          a.putCvpnCvpnid(null, (data, error) => {
            try {
              const displayE = 'cvpnid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putCvpnCvpnid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCvpnCvpnid - errors', () => {
      it('should have a deleteCvpnCvpnid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCvpnCvpnid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cvpnid', (done) => {
        try {
          a.deleteCvpnCvpnid(null, (data, error) => {
            try {
              const displayE = 'cvpnid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteCvpnCvpnid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidCvpns - errors', () => {
      it('should have a getOrgOrgidCvpns function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidCvpns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidCvpns(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidCvpns', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidCvpns - errors', () => {
      it('should have a postOrgOrgidCvpns function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidCvpns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidCvpns(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidCvpns', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cvpn', (done) => {
        try {
          a.postOrgOrgidCvpns('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cvpn is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidCvpns', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCvpnCvpnidNetid - errors', () => {
      it('should have a getCvpnCvpnidNetid function', (done) => {
        try {
          assert.equal(true, typeof a.getCvpnCvpnidNetid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cvpnid', (done) => {
        try {
          a.getCvpnCvpnidNetid(null, null, (data, error) => {
            try {
              const displayE = 'cvpnid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getCvpnCvpnidNetid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing netid', (done) => {
        try {
          a.getCvpnCvpnidNetid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'netid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getCvpnCvpnidNetid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCvpnCvpnidNetid - errors', () => {
      it('should have a putCvpnCvpnidNetid function', (done) => {
        try {
          assert.equal(true, typeof a.putCvpnCvpnidNetid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cvpnid', (done) => {
        try {
          a.putCvpnCvpnidNetid(null, null, (data, error) => {
            try {
              const displayE = 'cvpnid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putCvpnCvpnidNetid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing netid', (done) => {
        try {
          a.putCvpnCvpnidNetid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'netid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putCvpnCvpnidNetid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCvpnCvpnidNetid - errors', () => {
      it('should have a deleteCvpnCvpnidNetid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCvpnCvpnidNetid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cvpnid', (done) => {
        try {
          a.deleteCvpnCvpnidNetid(null, null, (data, error) => {
            try {
              const displayE = 'cvpnid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteCvpnCvpnidNetid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing netid', (done) => {
        try {
          a.deleteCvpnCvpnidNetid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'netid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteCvpnCvpnidNetid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAreas - errors', () => {
      it('should have a getAreas function', (done) => {
        try {
          assert.equal(true, typeof a.getAreas === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAreaAreaid - errors', () => {
      it('should have a getAreaAreaid function', (done) => {
        try {
          assert.equal(true, typeof a.getAreaAreaid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing areaid', (done) => {
        try {
          a.getAreaAreaid(null, (data, error) => {
            try {
              const displayE = 'areaid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getAreaAreaid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putAreaAreaid - errors', () => {
      it('should have a putAreaAreaid function', (done) => {
        try {
          assert.equal(true, typeof a.putAreaAreaid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing areaid', (done) => {
        try {
          a.putAreaAreaid(null, (data, error) => {
            try {
              const displayE = 'areaid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putAreaAreaid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAreaAreaid - errors', () => {
      it('should have a deleteAreaAreaid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAreaAreaid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing areaid', (done) => {
        try {
          a.deleteAreaAreaid(null, (data, error) => {
            try {
              const displayE = 'areaid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteAreaAreaid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidAreas - errors', () => {
      it('should have a getOrgOrgidAreas function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidAreas === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidAreas(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidAreas', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidAreas - errors', () => {
      it('should have a postOrgOrgidAreas function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidAreas === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidAreas(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidAreas', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing area', (done) => {
        try {
          a.postOrgOrgidAreas('fakeparam', null, (data, error) => {
            try {
              const displayE = 'area is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidAreas', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRealm - errors', () => {
      it('should have a getRealm function', (done) => {
        try {
          assert.equal(true, typeof a.getRealm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRealm - errors', () => {
      it('should have a putRealm function', (done) => {
        try {
          assert.equal(true, typeof a.putRealm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getShprimaryShprimaryid - errors', () => {
      it('should have a getShprimaryShprimaryid function', (done) => {
        try {
          assert.equal(true, typeof a.getShprimaryShprimaryid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing shprimaryid', (done) => {
        try {
          a.getShprimaryShprimaryid(null, (data, error) => {
            try {
              const displayE = 'shprimaryid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getShprimaryShprimaryid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putShprimaryShprimaryid - errors', () => {
      it('should have a putShprimaryShprimaryid function', (done) => {
        try {
          assert.equal(true, typeof a.putShprimaryShprimaryid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing shprimaryid', (done) => {
        try {
          a.putShprimaryShprimaryid(null, null, null, null, (data, error) => {
            try {
              const displayE = 'shprimaryid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putShprimaryShprimaryid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing shPrimaryIpv4', (done) => {
        try {
          a.putShprimaryShprimaryid('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'shPrimaryIpv4 is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putShprimaryShprimaryid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing shPrimaryGwv4', (done) => {
        try {
          a.putShprimaryShprimaryid('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'shPrimaryGwv4 is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putShprimaryShprimaryid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing net', (done) => {
        try {
          a.putShprimaryShprimaryid('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'net is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putShprimaryShprimaryid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInpathInpathid - errors', () => {
      it('should have a getInpathInpathid function', (done) => {
        try {
          assert.equal(true, typeof a.getInpathInpathid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing inpathid', (done) => {
        try {
          a.getInpathInpathid(null, (data, error) => {
            try {
              const displayE = 'inpathid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getInpathInpathid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putInpathInpathid - errors', () => {
      it('should have a putInpathInpathid function', (done) => {
        try {
          assert.equal(true, typeof a.putInpathInpathid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing inpathid', (done) => {
        try {
          a.putInpathInpathid(null, null, null, (data, error) => {
            try {
              const displayE = 'inpathid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putInpathInpathid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipv4', (done) => {
        try {
          a.putInpathInpathid('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ipv4 is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putInpathInpathid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing net', (done) => {
        try {
          a.putInpathInpathid('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'net is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putInpathInpathid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCepolicies - errors', () => {
      it('should have a getCepolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getCepolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCepoliciesCepolicyid - errors', () => {
      it('should have a getCepoliciesCepolicyid function', (done) => {
        try {
          assert.equal(true, typeof a.getCepoliciesCepolicyid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cepolicyid', (done) => {
        try {
          a.getCepoliciesCepolicyid(null, (data, error) => {
            try {
              const displayE = 'cepolicyid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getCepoliciesCepolicyid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCepoliciesCepolicyid - errors', () => {
      it('should have a putCepoliciesCepolicyid function', (done) => {
        try {
          assert.equal(true, typeof a.putCepoliciesCepolicyid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cepolicyid', (done) => {
        try {
          a.putCepoliciesCepolicyid(null, null, (data, error) => {
            try {
              const displayE = 'cepolicyid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putCepoliciesCepolicyid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cepolicy', (done) => {
        try {
          a.putCepoliciesCepolicyid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cepolicy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putCepoliciesCepolicyid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCepoliciesCepolicyid - errors', () => {
      it('should have a deleteCepoliciesCepolicyid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCepoliciesCepolicyid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cepolicyid', (done) => {
        try {
          a.deleteCepoliciesCepolicyid(null, (data, error) => {
            try {
              const displayE = 'cepolicyid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteCepoliciesCepolicyid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidCepolicy - errors', () => {
      it('should have a getOrgOrgidCepolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidCepolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidCepolicy(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidCepolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidCepolicy - errors', () => {
      it('should have a postOrgOrgidCepolicy function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidCepolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidCepolicy(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidCepolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cepolicy', (done) => {
        try {
          a.postOrgOrgidCepolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cepolicy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidCepolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCefilters - errors', () => {
      it('should have a getCefilters function', (done) => {
        try {
          assert.equal(true, typeof a.getCefilters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCefiltersCefilterid - errors', () => {
      it('should have a getCefiltersCefilterid function', (done) => {
        try {
          assert.equal(true, typeof a.getCefiltersCefilterid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cefilterid', (done) => {
        try {
          a.getCefiltersCefilterid(null, (data, error) => {
            try {
              const displayE = 'cefilterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getCefiltersCefilterid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCefiltersCefilterid - errors', () => {
      it('should have a putCefiltersCefilterid function', (done) => {
        try {
          assert.equal(true, typeof a.putCefiltersCefilterid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cefilterid', (done) => {
        try {
          a.putCefiltersCefilterid(null, null, (data, error) => {
            try {
              const displayE = 'cefilterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putCefiltersCefilterid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cefilter', (done) => {
        try {
          a.putCefiltersCefilterid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cefilter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putCefiltersCefilterid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCefiltersCefilterid - errors', () => {
      it('should have a deleteCefiltersCefilterid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCefiltersCefilterid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cefilterid', (done) => {
        try {
          a.deleteCefiltersCefilterid(null, (data, error) => {
            try {
              const displayE = 'cefilterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteCefiltersCefilterid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidCefilters - errors', () => {
      it('should have a getOrgOrgidCefilters function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidCefilters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidCefilters(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidCefilters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidCefilters - errors', () => {
      it('should have a postOrgOrgidCefilters function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidCefilters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidCefilters(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidCefilters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cefilter', (done) => {
        try {
          a.postOrgOrgidCefilters('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cefilter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidCefilters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPrefixes - errors', () => {
      it('should have a getPrefixes function', (done) => {
        try {
          assert.equal(true, typeof a.getPrefixes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPrefixesPrefixid - errors', () => {
      it('should have a getPrefixesPrefixid function', (done) => {
        try {
          assert.equal(true, typeof a.getPrefixesPrefixid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing prefixid', (done) => {
        try {
          a.getPrefixesPrefixid(null, (data, error) => {
            try {
              const displayE = 'prefixid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getPrefixesPrefixid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPrefixesPrefixid - errors', () => {
      it('should have a putPrefixesPrefixid function', (done) => {
        try {
          assert.equal(true, typeof a.putPrefixesPrefixid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing prefixid', (done) => {
        try {
          a.putPrefixesPrefixid(null, null, (data, error) => {
            try {
              const displayE = 'prefixid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putPrefixesPrefixid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing prefix', (done) => {
        try {
          a.putPrefixesPrefixid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'prefix is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putPrefixesPrefixid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePrefixesPrefixid - errors', () => {
      it('should have a deletePrefixesPrefixid function', (done) => {
        try {
          assert.equal(true, typeof a.deletePrefixesPrefixid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing prefixid', (done) => {
        try {
          a.deletePrefixesPrefixid(null, (data, error) => {
            try {
              const displayE = 'prefixid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deletePrefixesPrefixid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidPrefix - errors', () => {
      it('should have a getOrgOrgidPrefix function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidPrefix === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidPrefix(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidPrefix', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidPrefix - errors', () => {
      it('should have a postOrgOrgidPrefix function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidPrefix === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidPrefix(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidPrefix', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing prefix', (done) => {
        try {
          a.postOrgOrgidPrefix('fakeparam', null, (data, error) => {
            try {
              const displayE = 'prefix is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidPrefix', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCeexportrules - errors', () => {
      it('should have a getCeexportrules function', (done) => {
        try {
          assert.equal(true, typeof a.getCeexportrules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCeexportrulesCeexportruleid - errors', () => {
      it('should have a getCeexportrulesCeexportruleid function', (done) => {
        try {
          assert.equal(true, typeof a.getCeexportrulesCeexportruleid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ceexportruleid', (done) => {
        try {
          a.getCeexportrulesCeexportruleid(null, (data, error) => {
            try {
              const displayE = 'ceexportruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getCeexportrulesCeexportruleid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCeexportrulesCeexportruleid - errors', () => {
      it('should have a putCeexportrulesCeexportruleid function', (done) => {
        try {
          assert.equal(true, typeof a.putCeexportrulesCeexportruleid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ceexportruleid', (done) => {
        try {
          a.putCeexportrulesCeexportruleid(null, null, (data, error) => {
            try {
              const displayE = 'ceexportruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putCeexportrulesCeexportruleid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ceexportrule', (done) => {
        try {
          a.putCeexportrulesCeexportruleid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ceexportrule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putCeexportrulesCeexportruleid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCeexportrulesCeexportruleid - errors', () => {
      it('should have a deleteCeexportrulesCeexportruleid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCeexportrulesCeexportruleid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ceexportruleid', (done) => {
        try {
          a.deleteCeexportrulesCeexportruleid(null, (data, error) => {
            try {
              const displayE = 'ceexportruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteCeexportrulesCeexportruleid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidCeexportrules - errors', () => {
      it('should have a getOrgOrgidCeexportrules function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidCeexportrules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidCeexportrules(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidCeexportrules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidCeexportrules - errors', () => {
      it('should have a postOrgOrgidCeexportrules function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidCeexportrules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidCeexportrules(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidCeexportrules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ceexportrule', (done) => {
        try {
          a.postOrgOrgidCeexportrules('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ceexportrule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidCeexportrules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCeexportpolicies - errors', () => {
      it('should have a getCeexportpolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getCeexportpolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCeexportpoliciesCeexportpolicyid - errors', () => {
      it('should have a getCeexportpoliciesCeexportpolicyid function', (done) => {
        try {
          assert.equal(true, typeof a.getCeexportpoliciesCeexportpolicyid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ceexportpolicyid', (done) => {
        try {
          a.getCeexportpoliciesCeexportpolicyid(null, (data, error) => {
            try {
              const displayE = 'ceexportpolicyid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getCeexportpoliciesCeexportpolicyid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCeexportpoliciesCeexportpolicyid - errors', () => {
      it('should have a putCeexportpoliciesCeexportpolicyid function', (done) => {
        try {
          assert.equal(true, typeof a.putCeexportpoliciesCeexportpolicyid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ceexportpolicyid', (done) => {
        try {
          a.putCeexportpoliciesCeexportpolicyid(null, null, (data, error) => {
            try {
              const displayE = 'ceexportpolicyid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putCeexportpoliciesCeexportpolicyid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ceexportpolicy', (done) => {
        try {
          a.putCeexportpoliciesCeexportpolicyid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ceexportpolicy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putCeexportpoliciesCeexportpolicyid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCeexportpoliciesCeexportpolicyid - errors', () => {
      it('should have a deleteCeexportpoliciesCeexportpolicyid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCeexportpoliciesCeexportpolicyid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ceexportpolicyid', (done) => {
        try {
          a.deleteCeexportpoliciesCeexportpolicyid(null, (data, error) => {
            try {
              const displayE = 'ceexportpolicyid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteCeexportpoliciesCeexportpolicyid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidCeexportpolicies - errors', () => {
      it('should have a getOrgOrgidCeexportpolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidCeexportpolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidCeexportpolicies(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidCeexportpolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidCeexportpolicies - errors', () => {
      it('should have a postOrgOrgidCeexportpolicies function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidCeexportpolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidCeexportpolicies(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidCeexportpolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ceexportpolicy', (done) => {
        try {
          a.postOrgOrgidCeexportpolicies('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ceexportpolicy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidCeexportpolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCeimportpolicies - errors', () => {
      it('should have a getCeimportpolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getCeimportpolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCeimportpoliciesCeimportpolicyid - errors', () => {
      it('should have a getCeimportpoliciesCeimportpolicyid function', (done) => {
        try {
          assert.equal(true, typeof a.getCeimportpoliciesCeimportpolicyid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ceimportpolicyid', (done) => {
        try {
          a.getCeimportpoliciesCeimportpolicyid(null, (data, error) => {
            try {
              const displayE = 'ceimportpolicyid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getCeimportpoliciesCeimportpolicyid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCeimportpoliciesCeimportpolicyid - errors', () => {
      it('should have a putCeimportpoliciesCeimportpolicyid function', (done) => {
        try {
          assert.equal(true, typeof a.putCeimportpoliciesCeimportpolicyid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ceimportpolicyid', (done) => {
        try {
          a.putCeimportpoliciesCeimportpolicyid(null, null, (data, error) => {
            try {
              const displayE = 'ceimportpolicyid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putCeimportpoliciesCeimportpolicyid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ceimportpolicy', (done) => {
        try {
          a.putCeimportpoliciesCeimportpolicyid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ceimportpolicy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putCeimportpoliciesCeimportpolicyid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCeimportpoliciesCeimportpolicyid - errors', () => {
      it('should have a deleteCeimportpoliciesCeimportpolicyid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCeimportpoliciesCeimportpolicyid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ceimportpolicyid', (done) => {
        try {
          a.deleteCeimportpoliciesCeimportpolicyid(null, (data, error) => {
            try {
              const displayE = 'ceimportpolicyid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteCeimportpoliciesCeimportpolicyid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidCeimportpolicies - errors', () => {
      it('should have a getOrgOrgidCeimportpolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidCeimportpolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidCeimportpolicies(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidCeimportpolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidCeimportpolicies - errors', () => {
      it('should have a postOrgOrgidCeimportpolicies function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidCeimportpolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidCeimportpolicies(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidCeimportpolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ceimportpolicy', (done) => {
        try {
          a.postOrgOrgidCeimportpolicies('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ceimportpolicy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidCeimportpolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteSiteidCloudDeploy - errors', () => {
      it('should have a getSiteSiteidCloudDeploy function', (done) => {
        try {
          assert.equal(true, typeof a.getSiteSiteidCloudDeploy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteid', (done) => {
        try {
          a.getSiteSiteidCloudDeploy(null, (data, error) => {
            try {
              const displayE = 'siteid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getSiteSiteidCloudDeploy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSiteSiteidCloudDeploy - errors', () => {
      it('should have a postSiteSiteidCloudDeploy function', (done) => {
        try {
          assert.equal(true, typeof a.postSiteSiteidCloudDeploy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteid', (done) => {
        try {
          a.postSiteSiteidCloudDeploy(null, null, (data, error) => {
            try {
              const displayE = 'siteid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postSiteSiteidCloudDeploy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putSiteSiteidCloudDeploy - errors', () => {
      it('should have a putSiteSiteidCloudDeploy function', (done) => {
        try {
          assert.equal(true, typeof a.putSiteSiteidCloudDeploy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteid', (done) => {
        try {
          a.putSiteSiteidCloudDeploy(null, null, (data, error) => {
            try {
              const displayE = 'siteid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putSiteSiteidCloudDeploy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cloudDeploy', (done) => {
        try {
          a.putSiteSiteidCloudDeploy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cloudDeploy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putSiteSiteidCloudDeploy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSiteSiteidCloudDeploy - errors', () => {
      it('should have a deleteSiteSiteidCloudDeploy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSiteSiteidCloudDeploy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteid', (done) => {
        try {
          a.deleteSiteSiteidCloudDeploy(null, (data, error) => {
            try {
              const displayE = 'siteid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteSiteSiteidCloudDeploy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidCloudAccounts - errors', () => {
      it('should have a postOrgOrgidCloudAccounts function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidCloudAccounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidCloudAccounts(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidCloudAccounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cloudAccount', (done) => {
        try {
          a.postOrgOrgidCloudAccounts('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cloudAccount is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidCloudAccounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidCloudAccounts - errors', () => {
      it('should have a getOrgOrgidCloudAccounts function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidCloudAccounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidCloudAccounts(null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidCloudAccounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCloudAccountAccountId - errors', () => {
      it('should have a putCloudAccountAccountId function', (done) => {
        try {
          assert.equal(true, typeof a.putCloudAccountAccountId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cloudAccount', (done) => {
        try {
          a.putCloudAccountAccountId(null, null, (data, error) => {
            try {
              const displayE = 'cloudAccount is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putCloudAccountAccountId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.putCloudAccountAccountId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-putCloudAccountAccountId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCloudAccountAccountId - errors', () => {
      it('should have a deleteCloudAccountAccountId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCloudAccountAccountId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.deleteCloudAccountAccountId(null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteCloudAccountAccountId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudAccountAccountId - errors', () => {
      it('should have a getCloudAccountAccountId function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudAccountAccountId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getCloudAccountAccountId(null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getCloudAccountAccountId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudAccountAccountIdRefresh - errors', () => {
      it('should have a getCloudAccountAccountIdRefresh function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudAccountAccountIdRefresh === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getCloudAccountAccountIdRefresh(null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getCloudAccountAccountIdRefresh', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgOrgidCloudAccountAccountIdCloudConnectSubnets - errors', () => {
      it('should have a getOrgOrgidCloudAccountAccountIdCloudConnectSubnets function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgOrgidCloudAccountAccountIdCloudConnectSubnets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getOrgOrgidCloudAccountAccountIdCloudConnectSubnets(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidCloudAccountAccountIdCloudConnectSubnets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getOrgOrgidCloudAccountAccountIdCloudConnectSubnets('fakeparam', null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-getOrgOrgidCloudAccountAccountIdCloudConnectSubnets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId - errors', () => {
      it('should have a postOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.postOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetId', (done) => {
        try {
          a.postOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'subnetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId - errors', () => {
      it('should have a deleteOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.deleteOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId(null, null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.deleteOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetId', (done) => {
        try {
          a.deleteOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'subnetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteOrgOrgidCloudAccountAccountIdCloudConnectSubnetSubnetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId - errors', () => {
      it('should have a postOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.postOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.postOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId - errors', () => {
      it('should have a deleteOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.deleteOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId(null, null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.deleteOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.deleteOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteOrgOrgidCloudAccountAccountIdCloudConnectVpcVpcId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId - errors', () => {
      it('should have a postOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId function', (done) => {
        try {
          assert.equal(true, typeof a.postOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.postOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.postOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vnetId', (done) => {
        try {
          a.postOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vnetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-postOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId - errors', () => {
      it('should have a deleteOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.deleteOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId(null, null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.deleteOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vnetId', (done) => {
        try {
          a.deleteOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vnetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-steel_connect-adapter-deleteOrgOrgidCloudAccountAccountIdCloudConnectVnetVnetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
