# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Steel_connect System. The API that was used to build the adapter for Steel_connect is usually available in the report directory of this adapter. The adapter utilizes the Steel_connect API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Riverbed SteelConnect adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Riverbed’s SteelConnect controller to offer a cloud-scalable SD-WAN solution for configuration, orchestration and monitoring of an overlay network. 

With this adapter you have the ability to perform operations with Riverbed SteelConnect such as:

- Organization
- Site
- Zone
- Node
- Switch
- Access Point
- Endpoint
- Device

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
