# Riverbed SteelConnect

Vendor: Riverbed
Homepage: https://www.riverbed.com/

Product: Riverbed SteelConnect
Product Page: https://support.riverbed.com/bin/support/static/179ojgs83ca871oe9bpqhr3t30/html/fk6gkjg9hjl0e9b516mbasv2cf/sc_ug_html/index.html#page/scm/overview.html

## Introduction
We classify Riverbed SteelConnect into the SD-WAN/SASE domain as Riverbed SteelConnect provides a Software Defined Wide Area Network (SD-WAN) solution. 

"SteelConnect provides on-premises or cloud-based management system software for SD-WAN gateways, Wi-Fi access points, and Ethernet switches." 
"Unified connectivity and management across the WAN, remote LAN, and cloud networks." 

## Why Integrate
The Riverbed SteelConnect adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Riverbed’s SteelConnect controller to offer a cloud-scalable SD-WAN solution for configuration, orchestration and monitoring of an overlay network. 

With this adapter you have the ability to perform operations with Riverbed SteelConnect such as:

- Organization
- Site
- Zone
- Node
- Switch
- Access Point
- Endpoint
- Device

## Additional Product Documentation
The [API documents for Riverbed SteelConnect](https://support.riverbed.com/bin/support/static/9fq28fgkb3med9kc5so2r9a2v0/html/gs0er647apkcr6dra5ee6p74h8/sc_ug_html/index.html#page/sc_ug/api_intro.html)
